const $searchBtn = $('.js-search-btn');
const $firstLine = $('.header__first-line');
const $searchForm = $('.search-form');

$searchBtn.on('click', ()=> {
    $firstLine.addClass('js-search-form--active');
});


//hide search when clicked anywhere
$(document).on('click', function (event) {
    if ($(event.target).closest($searchForm).length === 0) {
        $firstLine.removeClass('js-search-form--active');           
    }
});