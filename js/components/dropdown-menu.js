$(document).ready(_=>{
    const $header = $('.js-dm-header');
    const $dm = $('.js-dm');  // dm for dropdown menu
    //const $dmContent = $('.js-dm-content')

    

    function addActive() {
        $dm.toggleClass('dropdown-menu--active');
    };
    const mq992 = window.matchMedia("(min-width: 62em)");
    function list(mq) {
        if(mq.matches) {
            //console.log('matches');
            $header.off();
            $dm.removeClass('dropdown-menu--active');           
        }
        else {
            //console.log("it doesnt");
            $header.on('click', addActive); 
        }
    }

    list(mq992);
    mq992.addListener(list);
    
    
});