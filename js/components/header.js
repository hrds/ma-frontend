$(document).ready(()=>{
    //DARK HEADER
    const $btnDropdown = $('.js-btn-dropdown');
    const $btnDropdownParent = $btnDropdown.parent();
    //console.log($btnDropdownParent[0])
    //console.log($btnDropdownParent);
    $btnDropdown.on('click', (e)=> {
        $(e.currentTarget).parent().toggleClass('active-dropdown');
    });

    $(document).on('click', (e)=>{
        if($(e.target).closest($($btnDropdownParent[0])).length == 0 && 
            $(e.target).closest($($btnDropdownParent[1])).length == 0)   {
            if ($($btnDropdownParent[0]).hasClass('active-dropdown')) { 
                $($btnDropdownParent[0]).removeClass('active-dropdown');           
            }

            if ($($btnDropdownParent[1]).hasClass('active-dropdown')) { 
                $($btnDropdownParent[1]).removeClass('active-dropdown');           
            }
        }
    });

    // mobile menu from left
    const $burger = $('.js-burger');
    const $mobileMenu = $('.dark-header');
    const $contentBox = $('.content-box'); 
    const $body = $('body');

    const mq992 = window.matchMedia('(min-width: 62em)');

    
    
    $burger.on('click', ()=> {
        $mobileMenu.toggleClass('dark-header--active');
        $contentBox.toggleClass('js-content-box-left');
        $body.toggleClass('overflow-x--hidden');
    });

    mq992.addListener(_=> {
        if(mq992.matches) {
            $mobileMenu.removeClass('dark-header--active');
            $contentBox.removeClass('js-content-box-left');
            $body.removeClass('overflow-x--hidden');
        }
    });


    /// fixed elements on scroll

    function fixed() {
        const $header = $('.js-header');
        const $main = $('main');

        $(window).scroll(_=>{
            let scrollPositionY = $(window).scrollTop();

            if(scrollPositionY >= 34) {
                $header.addClass('sticky-header');
                $main.addClass('fixed-header');
            

            }
            else {
                $header.removeClass('sticky-header');
                $main.removeClass('fixed-header');
            }
        });
    };

    fixed();
});

